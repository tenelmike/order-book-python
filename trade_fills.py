
import datetime

from order import Order

FILL_COLS = [
    'Date', 'Time', 'Price (USD)', 'Quantity', 'Trade ID', 'Bid Order ID', 'Ask Order ID'
]


class TradeFills:
    """
    Logging class to manage order fills and keep a log of the trades for display
    """

    def __init__(self):
        """
        Constructs the class and initializes values to zero
        """
        self.fills = []
        self.fill_id_num = 0

    def create_order_fill(self, bid_order: Order, ask_order: Order, price: float, quantity: int):
        """
        Creates a filled order when there is a match and adds it to the log of fills

        :param bid_order: The bid being matched
        :param ask_order: The ask being matched
        :param price: The price the match happened
        :param quantity: The quantity matched
        :return: void
        """
        timestamp = datetime.datetime.now()
        date_str = timestamp.strftime('%d-%m-%Y')
        time_str = timestamp.strftime('%H:%M:%S')

        self.fills.insert(0, {
            FILL_COLS[0]: date_str,
            FILL_COLS[1]: time_str,
            FILL_COLS[2]: price,
            FILL_COLS[3]: quantity,
            FILL_COLS[4]: self.fill_id_num,
            FILL_COLS[5]: bid_order.order_id,
            FILL_COLS[6]: ask_order.order_id
        })

        self.fill_id_num += 1
