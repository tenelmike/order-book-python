
from utils import get_order_match_quantity, update_books_from_match


class OrderBook:
    """
    Class implementing an OrderBook data structure and the associated methods and functions
    """

    def __init__(self):
        """
        Constructs the order book and sets the bid and ask side to their initial state. Sets the id counter
        for orders to zero
        """
        self.bid_book = []
        self.ask_book = []
        self.order_id_num = 0

    def increment_order_num(self):
        """
        Increments the order number so that it stays unique
        :return: void
        """
        self.order_id_num += 1

    def should_check_for_match(self):
        """
        Determines if the match orders function should run (only if there are orders that exist)
        :return: True if it should run, False otherwise
        """
        orders_exist = len(self.bid_book) > 0 and len(self.ask_book) > 0

        if not orders_exist:
            return False

        bid_order = self.bid_book[0]
        ask_order = self.ask_book[0]

        # if the spread isn't crossed there is no match
        if bid_order.price < ask_order.price:
            return False

        # if the spread is crossed
        if bid_order.price >= ask_order.price:
            return True
        else:
            return False

    def match_orders(self):
        """
        Matches orders in the order book and returns the price and quantity of the matches

        :return: List[[bid_order, ask_order, match_price, match_qty]]
        """

        match_list = []

        while self.should_check_for_match():

            bid_order = self.bid_book[0]
            ask_order = self.ask_book[0]

            # get the price and quantity of the match
            match_qty = get_order_match_quantity(bid_order, ask_order)

            # first in, first out so later order sets the price
            # if ask crossed the spread
            if ask_order.order_id > bid_order.order_id:
                match_price = bid_order.price
            # if bid crossed the spread
            else:
                match_price = ask_order.price

            # update the order book based on the quantities
            update_books_from_match(bid_order=bid_order, ask_order=ask_order,
                                    bid_book=self.bid_book, ask_book=self.ask_book)

            match_list.append([bid_order, ask_order, match_price, match_qty])

        return match_list

