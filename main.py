
from renderer import OrderBookRenderer
from book import OrderBook


if __name__ == '__main__':

    # create order book and start renderer
    order_book = OrderBook()
    renderer = OrderBookRenderer(order_book)
    renderer.loop()
