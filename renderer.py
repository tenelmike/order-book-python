
import datetime
import random

from tkinter import TclError

from order import Order
from book import OrderBook
from trade_fills import TradeFills
from utils import add_order_to_book
from frontend import draw_bids, draw_asks, draw_fills, draw_order_book_depth, launch_gui


def gui_cb(renderer):
    """
    Uses the tk function after to schedule a new order using a callback. Also calls the main update functions for
    the framework's windows

    :param renderer: The OrderBookRenderer
    :return: void
    """
    if not renderer.scheduled:
        renderer.main_window.after(renderer.order_period_ms, lambda: renderer.create_new_order())
        renderer.scheduled = True
    renderer.main_window.update_idletasks()
    renderer.main_window.update()


class OrderBookRenderer:
    """
    Uses the tkinter GUI framework to render an OrderBook
    """

    def __init__(self, order_book: OrderBook, order_period_ms=33):
        """
        Initializes the renderer for a given OrderBook object and period to generate orders
        :param order_book: The OrderBook to render
        :param order_period_ms: Time period to wait before generating a new random order
        """

        # if the renderer is running
        self.run = True

        # if a new order is scheduled by the gui callback
        self.scheduled = False

        # the order book being rendered
        self.order_book = order_book

        # log of trades
        self.fills = TradeFills()

        # time between new generated orders
        self.order_period_ms = order_period_ms

        # initializes the gui components using the helper function
        (self.main_window, self.bid_treeview, self.ask_treeview, self.fill_treeview,
         self.fig, self.fig_canvas) = launch_gui()

    def terminate(self):
        """
        Function allowing the renderer to be terminated gracefully by another thread or callback
        :return: void
        """
        self.run = False

    def create_new_order(self):
        """
        Creates a new order with random properties to add to the order book

        :return: void
        """
        timestamp = datetime.datetime.now().strftime('%H:%M:%S %d-%m-%Y')
        quantity = random.randint(1, 1000)
        price = round(random.normalvariate(100.0, 10.0), 2)
        limit = random.choice([True, False])
        bid = random.choice([True, False])

        if bid:
            add_order_to_book(
                Order(
                    timestamp=timestamp,
                    order_id=self.order_book.order_id_num,
                    quantity=quantity,
                    price=price,
                    limit=limit,
                    bid=bid
                ), same_side_book=self.order_book.bid_book, other_side_book=self.order_book.ask_book)
        else:
            add_order_to_book(
                Order(
                    timestamp=timestamp,
                    order_id=self.order_book.order_id_num,
                    quantity=quantity,
                    price=price,
                    limit=limit,
                    bid=bid
                ), same_side_book=self.order_book.ask_book, other_side_book=self.order_book.bid_book)

        self.order_book.increment_order_num()
        self.scheduled = False

    def loop(self, gui_callbacks=gui_cb):
        """
        The main renderer loop

        :param gui_callbacks: Any callbacks the gui needs to run. For tkinter there are some
        :return: void
        """
        while self.run:

            try:
                # check if the order book had matches
                match_list = self.order_book.match_orders()

                # create a fill if a trade went through
                for match in match_list:
                    [bid_order, ask_order, match_price, match_qty] = match
                    self.fills.create_order_fill(bid_order=bid_order, ask_order=ask_order,
                                                 price=match_price, quantity=match_qty)

                # render the bid table if the gui component exists and not running headless
                if self.bid_treeview is not None:
                    draw_bids(self.order_book.bid_book, self.bid_treeview)

                # render the ask table if the gui component exists and not running headless
                if self.ask_treeview is not None:
                    draw_asks(self.order_book.ask_book, self.ask_treeview)

                # render the table of filled orders if the gui component exists and not running headless
                if self.fill_treeview is not None:
                    draw_fills(self.fills.fills, self.fill_treeview)

                # render the order book depth plot if the gui component exists
                if self.fig is not None:
                    draw_order_book_depth(self.order_book, self.fig, self.fig_canvas)

                # call the gui callbacks if specified
                if gui_callbacks is not None:
                    gui_callbacks(self)
            except TclError:
                # if there are problems (like the gui X button being pressed) gracefully shut down the renderer
                self.terminate()
                pass
