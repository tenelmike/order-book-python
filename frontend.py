
import pandas as pd

import tkinter
from tkinter import Tk
from tkinter.ttk import LabelFrame, Treeview
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
import seaborn as sns

from typing import List, Tuple

from book import OrderBook
from trade_fills import FILL_COLS


def launch_gui() -> Tuple[Tk, Treeview, Treeview, Treeview, Figure, FigureCanvasTkAgg]:
    """
    Creates the GUI components we will use to display info to the user

    :return:
    """
    window = Tk()
    window.title('Demo Order Book')
    window.geometry('1100x1000')

    # define out bids, asks, and fills
    bid_panel = LabelFrame(window, text='Bids')
    ask_panel = LabelFrame(window, text='Asks')
    fill_panel = LabelFrame(window, text='Fills')
    _plot_panel = LabelFrame(window, text='Order Depth Plot')

    # specify the rows and columns
    window.rowconfigure(0, weight=1)
    window.rowconfigure(1, weight=1)
    window.rowconfigure(2, weight=1)
    window.rowconfigure(3, weight=1)
    window.columnconfigure(0, weight=1)
    window.columnconfigure(1, weight=1)

    # create a panel for displaying current bids by price and qty
    bid_tv = Treeview(bid_panel, columns=('Price', 'Quantity'), show='headings')
    bid_tv.heading('#1', text='Price')
    bid_tv.heading('#2', text='Quantity')
    bid_tv.pack(fill='both')
    bid_panel.grid(row=0, column=0, sticky='nsew')

    # create a panel for displaying current asks by price and qty
    ask_tv = Treeview(ask_panel, columns=('Price', 'Quantity'), show='headings')
    ask_tv.heading('#1', text='Price')
    ask_tv.heading('#2', text='Quantity')
    ask_tv.pack(fill='both')
    ask_panel.grid(row=0, column=1, sticky='nsew')

    fill_tv = Treeview(fill_panel, columns=(
        FILL_COLS[0], FILL_COLS[1], FILL_COLS[2],
        FILL_COLS[3], FILL_COLS[4], FILL_COLS[5],
        FILL_COLS[6]), show='headings')

    # set the column names
    fill_tv.heading("#1", text=FILL_COLS[0])
    fill_tv.heading("#2", text=FILL_COLS[1])
    fill_tv.heading("#3", text=FILL_COLS[2])
    fill_tv.heading("#4", text=FILL_COLS[3])
    fill_tv.heading("#5", text=FILL_COLS[4])
    fill_tv.heading("#6", text=FILL_COLS[5])
    fill_tv.heading("#7", text=FILL_COLS[6])

    # set the column sizes
    fill_tv.column("#1", width=150)
    fill_tv.column("#2", width=150)
    fill_tv.column("#3", width=150)
    fill_tv.column("#4", width=150)
    fill_tv.column("#5", width=150)
    fill_tv.column("#6", width=150)
    fill_tv.column("#7", width=150)

    fill_tv.pack(fill='both')
    fill_panel.grid(row=1, column=0, columnspan=2, sticky='nsew')

    # the figure that will contain the order book plot
    fig = Figure(figsize=(5, 5), dpi=100)

    # adding the subplot plotting the graph
    plot = fig.add_subplot(111)
    plot.plot([])

    # creating the Tkinter canvas
    # containing the Matplotlib figure
    fig_canvas = FigureCanvasTkAgg(fig, master=window)
    fig_canvas.draw()

    # placing the canvas on the Tkinter window
    fig_canvas.get_tk_widget().grid(row=2, column=0, columnspan=2, sticky='nsew')

    # create the panel for holding the plot
    toolbar_panel = LabelFrame(window)
    toolbar_panel.grid(row=3, column=0, columnspan=2, sticky='nsew')

    # creating the Matplotlib toolbar to zoom and control the plot
    toolbar = NavigationToolbar2Tk(fig_canvas, toolbar_panel)
    toolbar.pack(side=tkinter.TOP, fill=tkinter.X)

    return window, bid_tv, ask_tv, fill_tv, fig, fig_canvas


def draw_bids(bid_book: List, bid_tv: Treeview):
    """
    Renders the bid order book data structure in the frontend Treeview

    :param bid_book: The data structure
    :param bid_tv: The Treeview to render
    :return: void
    """
    bid_tv.delete(*bid_tv.get_children())

    if len(bid_book) == 0:
        return

    for i, bid in enumerate(bid_book):
        if bid is None:
            continue

        bid_tv.insert(parent='', index=i, iid=str(i), text='', values=(bid.price, bid.quantity))


def draw_asks(ask_book: List, ask_tv: Treeview):
    """
    Renders the ask order book data structure in the frontend Treeview

    :param ask_book: The data structure
    :param ask_tv: The Treeview to render
    :return: void
    """
    ask_tv.delete(*ask_tv.get_children())

    if len(ask_book) == 0:
        return

    for i, ask in enumerate(ask_book):
        if ask is None:
            continue

        ask_tv.insert(parent='', index=i, iid=str(i), text='', values=(ask.price, ask.quantity))


def draw_fills(fill_book: List, fill_tv: Treeview):
    """
    Renders the fill history data structure in the frontend Treeview

    :param fill_book: The data structure
    :param fill_tv: The Treeview to render
    :return: void
    """
    fill_tv.delete(*fill_tv.get_children())

    for i, fill in enumerate(fill_book):
        fill_tv.insert(parent='', index=i, iid=str(i), text='', values=(
            fill.get(FILL_COLS[0]), fill.get(FILL_COLS[1]),
            fill.get(FILL_COLS[2]), fill.get(FILL_COLS[3]), fill.get(FILL_COLS[4]),
            fill.get(FILL_COLS[5]), fill.get(FILL_COLS[6])
        ))


def draw_order_book_depth(order_book: OrderBook, fig: Figure, fig_canvas: FigureCanvasTkAgg):
    """
    Draws the plot of the order book depth based on the current state of the OrderBook

    :param order_book: The data to plot
    :param fig: The matplotlib fig to plot on
    :param fig_canvas: The canvas object to render the plot
    :return: void
    """
    # clear the old plot data
    fig.clf()

    # Set the title
    ax = fig.gca()
    ax.set_title('Order Book Depth')

    # create the ECDF plots for the order book
    # wrap the data structures in DataFrames for seaborn
    bid_df = pd.DataFrame(order_book.bid_book, columns=['timestamp', 'order_id', 'quantity', 'price', 'limit', 'bid'])
    ask_df = pd.DataFrame(order_book.ask_book, columns=['timestamp', 'order_id', 'quantity', 'price', 'limit', 'bid'])

    # plot bid side
    sns.ecdfplot(x='price', weights='quantity', stat='count', complementary=True, data=bid_df, ax=ax, color='g')

    # plot ask side
    sns.ecdfplot(x='price', weights='quantity', stat='count', data=ask_df, ax=ax, color='r')

    # Set the plot labels
    ax.set_xlabel('Price')
    ax.set_ylabel('Quantity')

    # draw the new plot
    fig_canvas.draw()
