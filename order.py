
from dataclasses import dataclass


@dataclass
class Order:
    """
    Order dataclass for storing info about a given bid or ask
    """
    timestamp: str
    order_id: int
    quantity: int
    price: float
    limit: bool
    bid: bool

    def __lt__(self, other):
        return self.price < other.price

    def __gt__(self, other):
        return self.price > other.price


def is_order_better_than(order, other_order):
    """
    Tests whether the order is a better price than the other order depending on bid or ask

    :param order: The Order to test
    :param other_order: The other Order
    :return: True if better, False otherwise
    """
    if order.bid:
        if order.price > other_order.price:
            return True
        else:
            return False
    else:
        if order.price > other_order.price:
            return False
        else:
            return True
