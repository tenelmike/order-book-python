
from typing import List

from order import Order, is_order_better_than


def get_order_match_quantity(bid: Order, ask: Order) -> int:
    """
    Takes a bid and an ask that are matched and returns the quantity of the match

    :param bid: The bid being matched
    :param ask: The ask being matched
    :return: The quantity matched
    """
    # get the number of shares matched based on the smaller order
    if bid.quantity < ask.quantity:
        return bid.quantity
    else:
        return ask.quantity


def remove_from_book(order_id: int, book: List):
    """
    Removes an order with the given id number from the specified book

    :param order_id: The id number to remove
    :param book: The book to remove the order from
    :return: void
    """
    to_remove = None
    for ordr in book:
        if ordr.order_id == order_id:
            to_remove = ordr
            break

    if to_remove is not None:
        book.remove(to_remove)


def reduce_book_quantity(order_id: int, book: List, quantity: int):
    """
    After a match in the order book, the side with remaining quantity needs to be reduced by the match amount

    :param order_id: The id of the order matched
    :param book: The book containing the order
    :param quantity: The matched amount to reduce the remaining order
    :return: void
    """
    for ordr in book:
        if ordr.order_id == order_id:
            ordr.quantity = ordr.quantity - quantity
            break


def update_books_from_match(bid_order: Order, ask_order: Order, bid_book: List, ask_book: List):
    """
    Updates the order books after a match using the matched bid and ask orders

    :param bid_order: The matched bid
    :param ask_order: The matched ask
    :param bid_book: The bid order book
    :param ask_book: The ask order book
    :return: void
    """
    if bid_order.quantity < ask_order.quantity:
        remove_from_book(bid_order.order_id, bid_book)
        reduce_book_quantity(ask_order.order_id, ask_book, bid_order.quantity)
    elif bid_order.quantity > ask_order.quantity:
        remove_from_book(ask_order.order_id, ask_book)
        reduce_book_quantity(bid_order.order_id, bid_book, ask_order.quantity)
    else:
        remove_from_book(bid_order.order_id, bid_book)
        remove_from_book(ask_order.order_id, ask_book)

    bid_book.sort(reverse=True)
    ask_book.sort()


def insert_order(order: Order, book: List):
    """
    Inserts an order into the data structure containing all of the orders in the book

    :param order: The order to insert
    :param book: The book data structure
    :return: void
    """
    # if it's the best order, put it first without sort
    if is_order_better_than(order, book[0]):
        book.insert(0, order)
    # if it's the worst order, put it last without sort
    elif not is_order_better_than(order, book[len(book) - 1]):
        book.append(order)
    # otherwise, loop to find the spot
    else:
        for ordr in book:
            if is_order_better_than(order, ordr):
                insert_point = book.index(ordr) - 1
                book.insert(insert_point, order)
                break

    if order.bid:
        book.sort(reverse=True)
    else:
        book.sort()


def add_order_to_book(order: Order, same_side_book: List, other_side_book: List):
    """
    Adds an order to the order book. If it is a limit order it is inserted, if it is a market order
    the behavior is slightly different since some will execute

    :param order: The order to add
    :param same_side_book: The book on the same side of the spread as the order
    :param other_side_book: The book across the spread as the order
    :return: void
    """
    if order.limit:
        # no orders exist, add it to the book
        if len(same_side_book) == 0:
            same_side_book.append(order)
        else:
            insert_order(order, same_side_book)
    else:
        if len(other_side_book) == 0:
            if len(same_side_book) == 0:
                same_side_book.append(order)
            else:
                order.price = same_side_book[0].price
                insert_order(order, same_side_book)
        else:
            order.price = other_side_book[0].price

            if len(same_side_book) == 0:
                same_side_book.append(order)
            else:
                insert_order(order, same_side_book)
