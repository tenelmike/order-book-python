
# Order Book Simulator in Python

Simple demo app for simulating an order book. Will randomly generate orders at a fixed interval and add them to the book, so if the generated order fills immediately it might take a bit for one side of the book to populate.

I am running locally using python 3.9 and poetry.

Run it with the standard:

poetry run main.py


Demo output:
![A screenshot of the GUI during operation](media/gui-demo.png)
